package com.example.WebViewTest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import java.net.URL;
import java.security.Key;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private Button button;
    private WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initSetting();
        initEvent();
    }

    // 设置view 初始化控件
    private void initView() {
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.btn_request);
        webView = (WebView) findViewById(R.id.webview);
    }

    /**** 设置webView的属性
     *
     * 从这个简单的例子，可以看出 java编写 Android例子的特色
     * 用getSettings方法获取 WebView setting属性
     * 然后调用WebSetting 的成员方法，设置setting里的各项属性
     */
    private void initSetting() {
        WebSettings ws = webView.getSettings();
        ws.setBuiltInZoomControls(true);
        ws.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        ws.setJavaScriptEnabled(true);
        ws.setSupportZoom(true);

        webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView wv, String  url) {
                webView.loadUrl(url);
                return true;
            }
            /***
             * url 类型： URL ，String
             */

        });
    }

    // 按键监听
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode,event);
    }

    // 初始化事件
    private void initEvent() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = editText.getText().toString().trim(); // trim 序列化
                webView.loadUrl(url);
            }
        });
    }
}
